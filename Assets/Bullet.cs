﻿using JetBrains.Annotations;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float _speed = 15f;
	public Transform _target;
	public float _damage = 1f;
	public float _radius = 0;
	public EnemyBehaviour _enemy;

	// Use this for initialization
	[UsedImplicitly]
	void Start()
	{
	}

	// Update is called once per frame
	[UsedImplicitly]
	void Update()
	{
		if (_target == null)
		{
			Destroy(gameObject);
			return;
		}
		Vector3 dir = _target.position - transform.localPosition;
		float distThisFrame = _speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame)
			BulletHit();
		else
		{
			//vis dar judam link Node'o
			transform.Translate(dir.normalized * distThisFrame, Space.World);
			//pasuka i judejimo krypti
			Quaternion targetRotation = Quaternion.LookRotation(dir);
			//susvelnina pasisukimus
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 5);
		}
	}

	public void BulletHit()
	{
		if (gameObject == null)
			return;
		if (_radius == 0)
		{
			//If the bullet is non explosive
			_target.GetComponent<EnemyBehaviour>().TakeDamage(_damage);
		}
		else
		{
			//If the bullet is explosive
			Collider[] colls = Physics.OverlapSphere(transform.position, _radius);

			foreach (Collider c in colls)
			{
				EnemyBehaviour e = c.GetComponent<EnemyBehaviour>();
				if (e != null)
				{
					//TODO: falloff dmg maybe?
					e.GetComponent<EnemyBehaviour>().TakeDamage(_damage);
				}
			}
		}
		Destroy(gameObject);
	}
}