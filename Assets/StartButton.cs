﻿using System.IO;
using Assets;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour {
	public Button _startButton;
	public Terrain _terrain;

	[UsedImplicitly]
	void Start () {
		Button btn = _startButton.GetComponent<Button>();
		btn.onClick.AddListener(OnStart);
	}

	void OnStart()
	{
		if (!SpawnManager.getInstance().IsSpawning())
		{
			StartCoroutine(SpawnManager.getInstance().DoSpawn(this, GameObject.Find("SpawnPoint")));
		}

		/*int xResolution = _terrain.terrainData.heightmapWidth;
		int	zResolution = _terrain.terrainData.heightmapHeight;
		float[,] heights = _terrain.terrainData.GetHeights(0,0,xResolution,zResolution);
		Debug.Log((1 / _terrain.terrainData.size.x) * xResolution);

		//int terX =(int)(-1.64 / _terrain.terrainData.size.x * xResolution);
		//int terZ =(int)(2.5 / _terrain.terrainData.size.z * zResolution);
		float y = heights[terX,terZ];
		y -= 0.001f;
		float[,] height = new float[1,1];
		height[0,0] = y;
		heights[terX,terZ] = y;
		_terrain.terrainData.SetHeights(terX, terZ, height);
		int pointX = 1;
		int pointY = 1;

		int terX =(int)((pointX / _terrain.terrainData.size.x) * xResolution);
		int terZ =(int)((pointY / _terrain.terrainData.size.z) * zResolution);
		float[,] height = _terrain.terrainData.GetHeights(terX - 4,terZ - 4,9,9);  //new float[1,1];

		for(int tempY = 0; tempY < 9; tempY++)
		for(int tempX = 0; tempX < 9; tempX++)
		{
			float dist_to_target = Mathf.Abs((float)tempY - 4f) + Mathf.Abs ((float)tempX - 4f);
			float maxDist = 8f;
			float proportion = dist_to_target / maxDist;

			height[tempX,tempY] += 0.01f * (1f - proportion);
			heights[terX - 4 + tempX,terZ - 4 + tempY] += 0.01f * (1f - proportion);
		}

		_terrain.terrainData.SetHeights(terX - 4, terZ - 4, height);*/
	}
}
