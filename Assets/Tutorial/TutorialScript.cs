﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour {

	public Canvas _TutorialStart;

	public Canvas _TutorialTurret;

	public Canvas _TutorialSpot;
	public Canvas _TutorialScore;

	public Canvas _StartWave;
	// Use this for initialization
	void Start ()
	{
		_TutorialStart = _TutorialStart.GetComponent<Canvas>();
		_TutorialTurret = _TutorialTurret.GetComponent<Canvas>();
		_StartWave = _StartWave.GetComponent<Canvas>();
		_TutorialSpot = _TutorialSpot.GetComponent<Canvas>();
		_TutorialScore = _TutorialScore.GetComponent<Canvas>();
		_TutorialTurret.enabled = false;
		_TutorialSpot.enabled = false;
		_TutorialScore.enabled = false;
		_StartWave.enabled = false;
	}

	public void ContinuePress()
	{
		_TutorialStart.enabled = false;
		_TutorialTurret.enabled = true;
	}
	
	public void TurretPress()
	{
		_TutorialTurret.enabled = false;
		_TutorialSpot.enabled = true;
		_TutorialScore.enabled = true;
	}

	public void TowerSpotPress()
	{
		_TutorialSpot.enabled = false;
		_TutorialScore.enabled = false;
		_StartWave.enabled = true;
	}

	public void StartWavePress()
	{
		_StartWave.enabled = false;
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
