﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets;

public class UserInterface : MonoBehaviour
{

	public Canvas _menu;
	
	// Use this for initialization
	void Start ()
	{
		_menu = _menu.GetComponent<Canvas>();
		_menu.enabled = false;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			_menu.enabled = true;
		}
	}

	public void OnYes()
	{
		SceneManager.LoadScene(0);
	}

	public void onNo()
	{
		_menu.enabled = false;
	}
}
