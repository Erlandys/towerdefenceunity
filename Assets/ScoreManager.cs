﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets;

public class ScoreManager : MonoBehaviour
{
	public int _lives = 20;
	public int _money = 100;

	public Text _moneyText;
	public Text _livesText;

	[UsedImplicitly]
	void Start ()
	{
		_lives = LevelsParser.getInstance().GetLevel().GetStartingLives();
		_money = LevelsParser.getInstance().GetLevel().GetStartingMoney();
	}

	public void LoseLife()
	{
		_lives -= 1;
		if (_lives <= 0)
			GameOver();
	}

	public void GameOver()
	{
		Debug.Log("Game Over");
		// TODO: Send player to game-over screen
		SceneManager.LoadScene(0);
		EnemiesParser.getInstance();
		WavesParser.getInstance();
	}

	[UsedImplicitly]
	void Update()
	{
		// TODO: Unoptimized. Doesn't need to update every frame
		_moneyText.text = "Money: " + _money;
		_livesText.text = "Lives: " + _lives;
	}
}