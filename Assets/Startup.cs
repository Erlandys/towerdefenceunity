﻿//using UnityEditor;
using UnityEngine;

namespace Assets
{
	public class Startup
	{
		[RuntimeInitializeOnLoadMethod]
		static void OnRuntimeMethodLoad()
		{
			EnemiesParser.getInstance();
			WavesParser.getInstance();
		}
	}
}