﻿using JetBrains.Annotations;
using UnityEngine;

public class TowerSpot : MonoBehaviour
{
	[UsedImplicitly]
	private void OnMouseUp()
	{
		BuildingManager bMgr = FindObjectOfType<BuildingManager>();
		if (bMgr.selectedTower != null)
		{
			ScoreManager sMgr = FindObjectOfType<ScoreManager>();
			if (sMgr._money < bMgr.selectedTower.GetComponent<Turret>()._price)
			{
				// Not enough money.
				return;
			}
			sMgr._money -= bMgr.selectedTower.GetComponent<Turret>()._price;

			//TODO: Now we assume that we're an object nested in parent

			Instantiate(bMgr.selectedTower, transform.parent.position, transform.parent.rotation);
			Destroy(transform.parent.gameObject);
		}
	}
}