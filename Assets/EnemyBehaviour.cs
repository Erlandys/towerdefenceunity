﻿using System;
using Assets.objects;
using JetBrains.Annotations;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
	private GameObject _path;

	private	Transform _targetPathNode;
	private	int _pathNodeInd;
	private Enemy _enemy;

	// Use this for initialization
	[UsedImplicitly]
	void Start()
	{
		_path = GameObject.Find("Pathing");
	}

	bool GetNextPathNode()
	{
		try
		{
			_targetPathNode = _path.transform.GetChild(_pathNodeInd);
			_pathNodeInd++;
		}
		catch (Exception)
		{
			GoalReached();
			return false;
		}
		return true;
	}

	// Update is called once per frame
	[UsedImplicitly]
	void Update()
	{
		if (_targetPathNode == null && !GetNextPathNode())
			return;
		Vector3 dir = _targetPathNode.position - transform.localPosition;
		float distThisFrame = _enemy.GetSpeed() * Time.deltaTime;

		if (dir.magnitude <= distThisFrame)
		{
			//pasiektas Node'as
			_targetPathNode = null;
		}
		else
		{
			//vis dar judam link Node'o
			transform.Translate(dir.normalized * distThisFrame, Space.World);
			//pasuka i judejimo krypti
			Quaternion targetRotation = Quaternion.LookRotation(dir);
			//susvelnina pasisukimus
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 5);
		}
	}

	void GoalReached()
	{
		//kai pasiekia paskutini node'a sunaikina priesa

		FindObjectOfType<ScoreManager>().LoseLife();
		Destroy(gameObject);
	}

	public void TakeDamage(float damage)
	{
		_enemy.DecreaseHealth(damage);
	}

	public void Die()
	{
		//TODO: Zodziu dabar labai abstrakciai aprasyta ir lengvai lauziama
		FindObjectOfType<ScoreManager>()._money += _enemy.GetMoneyReward();
		Destroy(gameObject);
	}

	public void SetEnemy(Enemy enemy)
	{
		_enemy = enemy;
	}
}