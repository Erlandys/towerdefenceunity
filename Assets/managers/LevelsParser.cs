﻿using System.Collections.Generic;
using System.Xml;
using Assets.objects;
using Assets.util;
using UnityEngine;

namespace Assets
{
	public class LevelsParser
	{
		private static LevelsParser _instance;
		private Dictionary<int, LevelTemplate> _templates;
		private LevelTemplate _currentLevel;

		public LevelsParser()
		{
			_templates = new Dictionary<int, LevelTemplate>();
			LoadLevels();
		}

		private void LoadLevels()
		{
			Debug.Log("Loading levels...");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("Assets/Data/Levels.xml");
			foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
			{
				if (node.Name == "level")
				{
					int id = int.Parse(node.Attributes["id"].Value);
					string name = node.Attributes["name"].Value;
					StatsSet set = new StatsSet();
					foreach (XmlNode innerNode in node.ChildNodes)
					{
						if (innerNode.Name == "set")
							set.set(innerNode.Attributes["name"].Value, innerNode.Attributes["value"].Value);
						else if (innerNode.Name == "minMax")
						{
							set.set(innerNode.Attributes["name"].Value + "Min", innerNode.Attributes["min"].Value);
							set.set(innerNode.Attributes["name"].Value + "Max", innerNode.Attributes["max"].Value);
						}
					}
					_templates.Add(id, new LevelTemplate(id, name, set));
				}
			}
			Debug.Log("Loaded " + _templates.Count + " level templates.");
		}

		public LevelTemplate GetTemplate(int id)
		{
			if (_templates.ContainsKey(id))
				return _templates[id];
			return null;
		}

		public void SetLevel(int id)
		{
			_currentLevel = GetTemplate(id);
		}

		public LevelTemplate GetLevel()
		{
			return _currentLevel;
		}

		public static LevelsParser getInstance()
		{
			if (_instance == null)
				_instance = new LevelsParser();
			return _instance;
		}
	}
}