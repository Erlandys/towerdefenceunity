﻿using System;
using System.Collections;
using Assets.objects;
using UnityEngine;

namespace Assets
{
	public class SpawnManager
	{
		private static SpawnManager _instance;
		private int _wave;
		private Wave _currentWave;
		private bool _isSpawning;

		public IEnumerator DoSpawn(MonoBehaviour behaviour, GameObject spawnPoint)
		{
			_isSpawning = true;
			if (_currentWave == null)
				_currentWave = new Wave(WavesParser.getInstance().GetTemplate(++_wave));
			EnemyTemplate template = _currentWave.GetSpawnTemplate();
			if (template == null)
			{
				_currentWave = new Wave(WavesParser.getInstance().GetTemplate(++_wave));
				_isSpawning = false;
				yield break;
			}
			// TODO: Manage spawn.
			GameObject go = MonoBehaviour.Instantiate(Resources.Load(template.GetModel())) as GameObject;
			go.transform.position = spawnPoint.transform.position;
			EnemyBehaviour enemyBehaviour = go.GetComponent<EnemyBehaviour>();
			enemyBehaviour.SetEnemy(new Enemy(template, enemyBehaviour, _currentWave));

			yield return new WaitForSeconds(_currentWave.GetSpawnDelay());
			behaviour.StartCoroutine(DoSpawn(behaviour, spawnPoint));
		}

		public Wave GetCurrentWave()
		{
			return _currentWave;
		}

		public bool IsSpawning()
		{
			return _isSpawning;
		}

		public void IsSpawning(bool spawning)
		{
			_isSpawning = spawning;
		}

		public static SpawnManager getInstance()
		{
			if (_instance == null)
				_instance = new SpawnManager();
			return _instance;
		}
	}
}