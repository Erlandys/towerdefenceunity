﻿using System.Collections.Generic;
using System.Xml;
using Assets.objects;
using Assets.util;
using UnityEngine;

namespace Assets
{
	public class EnemiesParser
	{
		private static EnemiesParser _instance;
		private Dictionary<int, EnemyTemplate> _templates;

		public EnemiesParser()
		{
			_templates = new Dictionary<int, EnemyTemplate>();
			LoadEnemies();
		}

		private void LoadEnemies()
		{
			Debug.Log("Loading enemies...");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("Assets/Data/Enemies.xml");
			foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
			{
				if (node.Name == "enemy")
				{
					int id = int.Parse(node.Attributes["id"].Value);
					string name = node.Attributes["name"].Value;
					StatsSet set = new StatsSet();
					foreach (XmlNode innerNode in node.ChildNodes)
					{
						if (innerNode.Name == "set")
							set.set(innerNode.Attributes["name"].Value, innerNode.Attributes["value"].Value);
						else if (innerNode.Name == "minMax")
						{
							set.set(innerNode.Attributes["name"].Value + "Min", innerNode.Attributes["min"].Value);
							set.set(innerNode.Attributes["name"].Value + "Max", innerNode.Attributes["max"].Value);
						}
					}
					_templates.Add(id, new EnemyTemplate(id, name, set));
				}
			}
			Debug.Log("Loaded " + _templates.Count + " enemy templates.");
		}

		public EnemyTemplate GetTemplate(int id)
		{
			if (_templates.ContainsKey(id))
				return _templates[id];
			return null;
		}

		public static EnemiesParser getInstance()
		{
			if (_instance == null)
				_instance = new EnemiesParser();
			return _instance;
		}
	}
}