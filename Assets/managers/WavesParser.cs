﻿using System.Collections.Generic;
using System.Xml;
using Assets.objects;
using Assets.util;
using UnityEngine;

namespace Assets
{
	public class WavesParser
	{
		private static WavesParser _instance;
		private Dictionary<int, WaveTemplate> _waves;

		public WavesParser()
		{
			_waves = new Dictionary<int, WaveTemplate>();
			LoadWaves();
		}

		private void LoadWaves()
		{
			Debug.Log("Loading waves...");
			XmlDocument xmlDoc= new XmlDocument();
			xmlDoc.Load("Assets/Data/Waves.xml"); // Load the XML document from the specified file
			foreach(XmlNode node in xmlDoc.DocumentElement.ChildNodes) {
				if (node.Name == "wave")
				{
					int id = int.Parse(node.Attributes["id"].Value);
					StatsSet set = new StatsSet();
					List<object[]> enemies = new List<object[]>();
					foreach (XmlNode innerNode in node.ChildNodes)
					{
						if (innerNode.Name == "enemy")
						{
							int enemyId = int.Parse(innerNode.Attributes["id"].Value);
							EnemyTemplate eTemplate = EnemiesParser.getInstance().GetTemplate(enemyId);
							if (eTemplate == null)
							{
								Debug.Log("Enemy with id[" + enemyId + "] not found!");
								continue;
							}
							int minCount = int.Parse(innerNode.Attributes["minCount"].Value);
							int maxCount = int.Parse(innerNode.Attributes["maxCount"].Value);
							enemies.Add(new object[] {eTemplate, minCount, maxCount});
						}
						else if (innerNode.Name == "set")
							set.set(innerNode.Attributes["name"].Value, innerNode.Attributes["value"].Value);
						else if (innerNode.Name == "minMax")
						{
							set.set(innerNode.Attributes["name"].Value + "Min", innerNode.Attributes["min"].Value);
							set.set(innerNode.Attributes["name"].Value + "Max", innerNode.Attributes["max"].Value);
						}
					}
					_waves.Add(id, new WaveTemplate(id, set, enemies));
				}
			}
			Debug.Log("Loaded " + _waves.Count + " wave templates.");
		}

		public WaveTemplate GetTemplate(int id)
		{
			if (_waves.ContainsKey(id))
				return _waves[id];
			return null;
		}

		public static WavesParser getInstance()
		{
			if (_instance == null)
				_instance = new WavesParser();
			return _instance;
		}
	}
}