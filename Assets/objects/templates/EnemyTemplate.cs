﻿using Assets.util;

namespace Assets.objects
{
	public class EnemyTemplate : ObjectTemplate
	{
		private float _speed;
		private float _health;
		private int _moneyRewardMin, _moneyRewardMax;
		private int _pointsRewardMin, _pointsRewardMax;

		public EnemyTemplate(int id, string name, StatsSet set) : base(id, name, set)
		{
			_speed = set.getFloat("speed");
			_health = set.getFloat("health");
			_moneyRewardMin = set.getInteger("moneyMin");
			_moneyRewardMax = set.getInteger("moneyMax");
			_pointsRewardMin = set.getInteger("pointsMin");
			_pointsRewardMax = set.getInteger("pointsMax");
		}

		public float GetSpeed()
		{
			return _speed;
		}

		public float GetHealth()
		{
			return _health;
		}

		public int GetMinMoneyReward()
		{
			return _moneyRewardMin;
		}

		public int GetMaxMoneyReward()
		{
			return _moneyRewardMax;
		}

		public int GetMinPointsReward()
		{
			return _pointsRewardMin;
		}

		public int GetMaxPointsReward()
		{
			return _pointsRewardMax;
		}
	}
}