﻿using System.Collections.Generic;
using Assets.util;

namespace Assets.objects
{
	public class WaveTemplate
	{
		private int _id;
		private int _timeBetweenSpawnsMin;
		private int _timeBetweenSpawnsMax;
		private float _multiplyPoints;
		private float _multiplyMoney;
		private float _multiplyHealth;
		private List<object[]> _enemies;

		public WaveTemplate(int id, StatsSet set, List<object[]> enemies)
		{
			_id = id;
			_timeBetweenSpawnsMin = set.getInteger("timeBetweenSpawnsMin");
			_timeBetweenSpawnsMax = set.getInteger("timeBetweenSpawnsMax");
			_multiplyPoints = set.getFloat("multiplyPoints", 1.0f);
			_multiplyMoney = set.getFloat("multiplyMoney", 1.0f);
			_multiplyHealth = set.getFloat("multiplyHealth", 1.0f);
			_enemies = enemies;
		}

		public int GetId()
		{
			return _id;
		}

		public int GetTimeBetweenSpawnsMin()
		{
			return _timeBetweenSpawnsMin;
		}

		public int GetTimeBetweenSpawnsMax()
		{
			return _timeBetweenSpawnsMax;
		}

		public float GetPointsMultiplier()
		{
			return _multiplyPoints;
		}

		public float GetMoneyMultiplier()
		{
			return _multiplyMoney;
		}

		public float GetHealthMultiplier()
		{
			return _multiplyHealth;
		}

		public List<object[]> GetEnemiesList()
		{
			return _enemies;
		}
	}
}