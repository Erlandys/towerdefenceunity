﻿using Assets.util;

namespace Assets.objects
{
	public abstract class ObjectTemplate
	{
		private int _id;
		private string _name;
		private string _model;

		public ObjectTemplate(int id, string name, StatsSet set)
		{
			_id = id;
			_name = name;
			_model = set.getString("model", "");
		}

		public int GetId()
		{
			return _id;
		}

		public string GetName()
		{
			return _name;
		}

		public string GetModel()
		{
			return _model;
		}
	}
}