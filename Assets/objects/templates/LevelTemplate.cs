﻿using Assets.util;

namespace Assets.objects
{
	public class LevelTemplate : ObjectTemplate
	{
		private float _multiplySpeed;
		private float _multiplyHealth;
		private float _multiplyMoney;
		private float _multiplyPoints;
		private int _startingMoney;
		private int _startingLives;

		public LevelTemplate(int id, string name, StatsSet set) : base(id, name, set)
		{
			_multiplySpeed = set.getFloat("multiplySpeed");
			_multiplyHealth = set.getFloat("multiplyHealth");
			_multiplyMoney = set.getFloat("multiplyMoney");
			_multiplyPoints = set.getFloat("multiplyPoints");
			_startingMoney = set.getInteger("startingMoney");
			_startingLives = set.getInteger("startingLives");
		}

		public float GetSpeedMultiplier()
		{
			return _multiplySpeed;
		}

		public float GetHealthMultiplier()
		{
			return _multiplyHealth;
		}

		public float GetMoneyMultiplier()
		{
			return _multiplyMoney;
		}

		public float GetPointsMultiplier()
		{
			return _multiplyPoints;
		}

		public int GetStartingMoney()
		{
			return _startingMoney;
		}

		public int GetStartingLives()
		{
			return _startingLives;
		}

	}
}