﻿using UnityEngine;

namespace Assets.objects
{
	public class Enemy
	{
		private float _speed;
		private float _health;
		private int _moneyReward;
		private int _pointsReward;

		private EnemyBehaviour _behaviour;

		public Enemy(EnemyTemplate template, EnemyBehaviour behaviour, Wave currentWave)
		{
			_speed = template.GetSpeed();
			_health = template.GetHealth() * currentWave.GetHealthMultiplier();
			float minMoneyReward = template.GetMinMoneyReward() * currentWave.GetMoneyMultiplier();
			float maxMoneyReward = template.GetMaxMoneyReward() * currentWave.GetMoneyMultiplier();
			float minPointsReward = template.GetMinPointsReward() * currentWave.GetPointsMultiplier();
			float maxPointsReward = template.GetMaxPointsReward() * currentWave.GetPointsMultiplier();
			_behaviour = behaviour;

			_speed *= LevelsParser.getInstance().GetLevel().GetSpeedMultiplier();
			_health *= LevelsParser.getInstance().GetLevel().GetHealthMultiplier();
			minMoneyReward *= LevelsParser.getInstance().GetLevel().GetMoneyMultiplier();
			maxMoneyReward *= LevelsParser.getInstance().GetLevel().GetMoneyMultiplier();
			minPointsReward *= LevelsParser.getInstance().GetLevel().GetPointsMultiplier();
			maxPointsReward *= LevelsParser.getInstance().GetLevel().GetPointsMultiplier();


			_moneyReward = (int)Random.Range(minMoneyReward, maxMoneyReward);
			_pointsReward = (int)Random.Range(minPointsReward, maxPointsReward);
		}

		public float GetSpeed()
		{
			return _speed;
		}

		public void DecreaseHealth(float health)
		{
			_health -= health;
			if (_health <= 0)
				_behaviour.Die();
		}

		public int GetMoneyReward()
		{
			return _moneyReward;
		}

		public int GetPointsReward()
		{
			return _pointsReward;
		}
	}
}