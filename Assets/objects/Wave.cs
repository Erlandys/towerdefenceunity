﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.objects
{
	public class Wave
	{
		private WaveTemplate _template;
		private List<EnemyTemplate> _enemies;
		private List<int> _enemiesLeft;

		public Wave(WaveTemplate template)
		{
			_enemies = new List<EnemyTemplate>();
			_enemiesLeft = new List<int>();
			foreach (object[] enemy in template.GetEnemiesList())
			{
				_enemies.Add((EnemyTemplate) enemy[0]);
				_enemiesLeft.Add(Random.Range((int) enemy[1], (int) enemy[2]));
			}
			_template = template;
		}

		public float GetSpawnDelay()
		{
			return Random.Range(_template.GetTimeBetweenSpawnsMin(), _template.GetTimeBetweenSpawnsMax()) / 1000.0f;
		}

		public float GetMoneyMultiplier()
		{
			return _template.GetMoneyMultiplier();
		}

		public float GetPointsMultiplier()
		{
			return _template.GetPointsMultiplier();
		}

		public float GetHealthMultiplier()
		{
			return _template.GetHealthMultiplier();
		}

		public EnemyTemplate GetSpawnTemplate()
		{
			if (_enemies.Count < 1)
				return null;
			int enemyIndex = Random.Range(0, _enemies.Count);
			_enemiesLeft[enemyIndex]--;
			EnemyTemplate template = _enemies[enemyIndex];
			if (_enemiesLeft[enemyIndex] < 1)
			{
				_enemies.RemoveAt(enemyIndex);
				_enemiesLeft.RemoveAt(enemyIndex);
			}
			return template;
		}
	}
}