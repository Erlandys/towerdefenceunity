﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets;

public class menuScript : MonoBehaviour
{
	public Canvas _QuitMenu;

	public Button _StartButton;

	public Button _ExitButton;

	public Canvas _LevelMenu;
	// Use this for initialization
	
	void Start ()
	{
		EnemiesParser.getInstance();
		WavesParser.getInstance();
		LevelsParser.getInstance();
		SpawnManager.getInstance().IsSpawning(false);
		_QuitMenu = _QuitMenu.GetComponent<Canvas>();
		_LevelMenu = _LevelMenu.GetComponent<Canvas>();
		_StartButton = _StartButton.GetComponent<Button>();
		_ExitButton = _ExitButton.GetComponent<Button>();
		_LevelMenu.enabled = false;
		_QuitMenu.enabled = false;
	}

	public void EasyPress()
	{
		LevelsParser.getInstance().SetLevel(1);
		SceneManager.LoadScene(1);
	}
	
	public void MediumPress()
	{
		LevelsParser.getInstance().SetLevel(2);
		SceneManager.LoadScene(2);
	}

	public void HardPress()
	{
		LevelsParser.getInstance().SetLevel(3);
		SceneManager.LoadScene(3);
	}
	
	public void TutorialPress()
	{
		LevelsParser.getInstance().SetLevel(4);
		SceneManager.LoadScene(4);
	}

	public void ExitPress()
	{
		_QuitMenu.enabled = true;
		_StartButton.enabled = false;
		_ExitButton.enabled = false;
	}

	public void NoPress()
	{
		_QuitMenu.enabled = false;
		_StartButton.enabled = true;
		_ExitButton.enabled = true;
	}

	public void BackPress()
	{
		_LevelMenu.enabled = false;
		_StartButton.enabled = true;
		_ExitButton.enabled = true;
	}

	public void StartLevel()
	{
		_LevelMenu.enabled = true;
		_StartButton.enabled = false;
		_ExitButton.enabled = false;
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
