﻿using JetBrains.Annotations;
using UnityEngine;

public class Turret : MonoBehaviour
{
	Transform _turretTransform;

	private Animation anim;

	private float _range = 10f;
	public GameObject _bullet;

	public int _price = 20;

	private	float _cooldownLeft;

	public float _damage = 1;
	public float _radius = 0;
	public float _cooldown = 0.5f;

	// Use this for initialization
	[UsedImplicitly]
	void Start()
	{
		_turretTransform = transform.Find("Canon");
		anim = GetComponent<Animation>();
	}

	// Update is called once per frame
	[UsedImplicitly]
	void Update()
	{
		//TODO: Optimizavimas
		EnemyBehaviour[] enemies = FindObjectsOfType<EnemyBehaviour>();
		EnemyBehaviour arciausias = null;
		float dist = Mathf.Infinity;

		foreach (EnemyBehaviour e in enemies)
		{
			float d = Vector3.Distance(transform.position, e.transform.position);
			if (arciausias == null || d < dist)
			{
				arciausias = e;
				dist = d;
			}
		}
		if (arciausias == null)
		{
			//Debug.Log("Nėra priešų");
			return;
		}
		Vector3 dir = arciausias.transform.position - transform.position;
		if (dir.magnitude <= _range)
		{
			Quaternion lookRotation = Quaternion.LookRotation(dir);
			if (_turretTransform == null)
			{
				//Debug.Log("Kazkodel turret transform yran ulis.");
				return;
			}
			_turretTransform.rotation = Quaternion.Euler(-90, lookRotation.eulerAngles.y, 0);

			_cooldownLeft -= Time.deltaTime;
			if (_cooldownLeft <= 0)
			{
				_cooldownLeft = _cooldown;
				ShootAt(arciausias);
			}
		}
	}

	void ShootAt(EnemyBehaviour e)
	{
		GameObject bulletGO = Instantiate(_bullet, transform.position, transform.rotation);

		anim.Play();

		Bullet b = bulletGO.GetComponent<Bullet>();
		b._target = e.transform;
		b._enemy = e;
		b._damage = _damage;
		b._radius = _radius;
	}
}
