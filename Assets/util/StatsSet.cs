﻿using System;
using System.Collections.Generic;

namespace Assets.util
{
	public class StatsSet : Dictionary<string, object>
	{
		public void set(string key, object value)
		{
			Add(key, value);
		}
	
		public void set(string key, string value)
		{
			Add(key, value);
		}
	
		public void set(string key, bool value)
		{
			Add(key, value);
		}
	
		public void set(string key, int value)
		{
			Add(key, value);
		}
	
		public void set(string key, int[] value)
		{
			Add(key, value);
		}
	
		public void set(string key, long value)
		{
			Add(key, value);
		}
	
		public void set(string key, double value)
		{
			Add(key, value);
		}

		public bool getBool(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : bool value required, but not found for key: " + key + ".");

			object val = this[key];

			if (val is bool)
				return (bool) val;
			if (val is string)
				return bool.Parse((string) val);
			if (val is int)
				return int.Parse((string) val) != 0;

			throw new ArgumentException("StatsSet : bool value required, but found: " + val + " for key: " + key + ".");
		}

		public bool getBool(string key, bool defaultValue)
		{
			object val = this[key];

			if (val is bool)
				return (bool) val;
			if (val is string)
				return bool.Parse((string) val);
			if (val is int)
				return int.Parse((string) val) != 0;

			return defaultValue;
		}

		public byte getByte(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : bool value required, but not found for key: " + key + ".");

			object val = this[key];

			if (val is byte)
				return (byte) val;
			if (val is string)
				return byte.Parse((string) val);

			throw new ArgumentException("StatsSet : bool value required, but found: " + val + " for key: " + key + ".");
		}

		public byte getByte(string key, byte defaultValue)
		{
			if (!ContainsKey(key))
				return defaultValue;

			object val = this[key];

			if (val is byte)
				return (byte) val;
			if (val is string)
				return byte.Parse((string) val);

			return defaultValue;
		}

		public double getDouble(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : Double value required, but not found for key: " + key + ".");

			object val = this[key];

			if (val is double)
				return (double) val;
			if (val is string)
				return double.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			throw new ArgumentException("StatsSet : Double value required, but found: " + val + " for key: " + key + ".");
		}

		public double getDouble(string key, double defaultValue)
		{
			if (!ContainsKey(key))
				return defaultValue;

			object val = this[key];

			if (val is double)
				return (double) val;
			if (val is string)
				return double.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			return defaultValue;
		}

		public float getFloat(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : Float value required, but not found for key: " + key + ".");

			object val = this[key];

			if (val is float)
				return (float) val;
			if (val is string)
				return float.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			throw new ArgumentException("StatsSet : Float value required, but found: " + val + " for key: " + key + ".");
		}

		public float getFloat(string key, float defaultValue)
		{
			if (!ContainsKey(key))
				return defaultValue;

			object val = this[key];

			if (val is float)
				return (float) val;
			if (val is string)
				return float.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			return defaultValue;
		}

		public int getInteger(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : Integer value required, but not found for key: " + key + ".");

			object val = this[key];

			if (val is int)
				return (int) val;
			if (val is string)
				return int.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			throw new ArgumentException("StatsSet : Integer value required, but found: " + val + " for key: " + key + ".");
		}

		public int getInteger(string key, int defaultValue)
		{
			if (!ContainsKey(key))
				return defaultValue;

			object val = this[key];

			if (val is int)
				return (int) val;
			if (val is string)
				return int.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			return defaultValue;
		}

		public long getLong(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : Long value required, but not found for key: " + key + ".");

			Object val = this[key];

			if (val is long)
				return (long) val;
			if (val is string)
				return long.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			throw new ArgumentException("StatsSet : Long value required, but found: " + val + " for key: " + key + ".");
		}

		public long getLong(string key, long defaultValue)
		{
			if (!ContainsKey(key))
				return defaultValue;

			Object val = this[key];

			if (val is long)
				return (long) val;
			if (val is string)
				return long.Parse((string) val);
			if (val is bool)
				return (bool) val ? 1 : 0;

			return defaultValue;
		}

		public String getString(string key)
		{
			if (!ContainsKey(key))
				throw new ArgumentException("StatsSet : String value required, but not found for key: " + key + ".");

			Object val = this[key];

			if (val != null)
				return val.ToString();

			throw new ArgumentException("StatsSet : String value required, but unspecified for key: " + key + ".");
		}

		public String getString(string key, string defaultValue)
		{
			if (!ContainsKey(key))
				return defaultValue;

			Object val = this[key];

			if (val != null)
				return val.ToString();

			return defaultValue;
		}
	}
}