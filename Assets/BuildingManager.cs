﻿using JetBrains.Annotations;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
	public GameObject selectedTower;

	// Use this for initialization
	[UsedImplicitly]
	void Start()
	{
	}

	// Update is called once per frame
	[UsedImplicitly]
	void Update()
	{
	}

	public void SelectTowerType(GameObject prefab)
	{
		selectedTower = prefab;
	}
}